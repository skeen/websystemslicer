/// <reference path="../ICuraEngine.ts" />

class CuraEngineWorker implements ICuraEngine
{
    private stdout : string;
    private stderr : string;
    private status : string;
    private gcode : string;

    private worker : any;
    private loaded : boolean;
    private call_id : number;
    private callback_table : Object;

    constructor(engine_online ?: Function, webworker_script ?: string)
    {
        this.stdout = "";
        this.stderr = "";
        this.status = "";
        this.gcode = ""; 

        this.loaded = false;
        this.call_id = 0;
        this.callback_table = {
            0: engine_online
        };

        if(webworker_script != null)
        {
            this.worker = new Worker(webworker_script);
        }
        else
        {
            this.worker = new Worker("CuraEngineWebWorker.js");
        }
        var _this = this;
        this.worker.onmessage = function(e)
        {
            var id = e.data.id;
            var callback = _this.callback_table[id];
            if(callback) {
                callback(e.data.other);
                _this.callback_table[id] = null;
            }
        }
    }

    private dispatch(to_call : string, payload : any, callback ?: Function) : void
    {
        this.call_id = this.call_id + 1;
        var message = {
            func: to_call,
            data: payload,
            id: this.call_id
        };
        this.callback_table[this.call_id] = callback;

        this.worker.postMessage(message);
    }

    is_loaded() : boolean
    {
        return this.loaded;
    }

    load(callback ?: Function) : void
    {
        var _this = this;
        this.dispatch('load', [], function()
        {
            _this.loaded = true;
            callback();
        });
    }

    raw_run(argv ?: string[], callback ?: Function) : void
    {
        var _this = this;
        this.dispatch('raw_run', argv, function(payload)
        {
            _this.stdout = payload.stdout;
            _this.stderr = payload.stderr;
            _this.status = payload.status;
            _this.gcode = payload.gcode;
            callback();
        });
    }

    run(callback ?: Function) : void
    {
        this.raw_run(['a.stl', '-o', 'a.gcode'], callback);
    }

    get_stdout() : string
    {
        return this.stdout;
    }

    get_stderr() : string
    {
        return this.stderr;
    }

    get_status() : string
    {
        return this.status;
    }

    get_gcode() : string
    {
        return this.gcode;
    }

    save_ascii_model(ascii : string, callback ?: Function) : void
    {
        this.dispatch('save_ascii_model', ascii, callback);
    }

    save_binary_model(binary : Uint8Array, callback ?: Function) : void
    {
        this.dispatch('save_binary_model', binary, callback);
    }
}
