importScripts("http://requirejs.org/docs/release/2.1.15/minified/require.js");
importScripts("CuraEngine.js");

var engine = new CuraEngine();

function load(id)
{
    engine.load(function()
    {
        postMessage({id: id});
    });
}

function raw_run(id, payload)
{
    engine.raw_run(payload);
    var payload = {
        stdout: engine.get_stdout(),
        stderr: engine.get_stderr(),
        status: engine.get_status(),
        gcode: engine.get_gcode()
    }

    postMessage({id: id, other: payload});
}

function save_ascii_model(id, payload)
{
    engine.save_ascii_model(payload);
    postMessage({id: id});
}

function save_binary_model(id, payload)
{
    engine.save_binary_model(payload);
    postMessage({id: id});
}

onmessage = function(e)
{
    var func = e.data.func;
    var payload = e.data.data;
    var id = e.data.id;
    switch(func)
    {
        case "load":
            load(id, payload);
            break;
        case "raw_run":
            raw_run(id, payload);
            break;
        case "save_ascii_model":
            save_ascii_model(id, payload);
            break;
        case "save_binary_model":
            save_binary_model(id, payload);
            break;
    }
}

// Let the world know we're online
postMessage({id: 0});
