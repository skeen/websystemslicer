interface ICuraEngine
{
    // Is the underlying emscripten cura engine loaded?
    is_loaded() : boolean;
    // Load the underlying emscripten cura engine,
    // callback when loaded.
    load(callback ?: Function) : void;

    // Run the emscripten cura engine, with the commandlines in argv.
    // Callback when finished running (sync mode = callback just before return)
    raw_run(argv ?: string[], callback ?: Function) : void;
    // Run the emscripten cura engine, passing default commandline arguments.
    // Callback when finished running (sync mode = callback just before return)
    run(callback ?: Function) : void;

    // Get the captured stdout/stderr/status of last run
    get_stdout() : string;
    get_stderr() : string;
    get_status() : string;

    // Get the resulting gcode of last run (functional assuming calls via run())
    get_gcode() : string;

    // Save an ascii STL model for next execution, callback when saved
    save_ascii_model(ascii : string, callback ?: Function) : void;
    // Save a binary STL model for next execution, callback when saved
    save_binary_model(binary : Uint8Array, callback ?: Function) : void;
}
 
