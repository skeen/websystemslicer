/// <reference path="../CuraEngine.ts"/>
declare var require;

var engine = new CuraEngine();
engine.load(function()
{
    console.log("Loaded!");

    // At a later time
    console.log("Calling main!");
    //engine.raw_run(["-o", "a.gcode"]);
    //engine.raw_run(['a.stl']);
    engine.run();
    console.log("Main called!");
    console.log("Printing stderr:");
    console.log(engine.get_stderr());
    console.log("Printing stdout:");
    console.log(engine.get_stdout());
    console.log("Printing status:");
    console.log(engine.get_status());
    console.log("Printing gcode:");
    console.log(engine.get_gcode());
    
    var fs = require('fs');
    var model_data = fs.readFileSync("build/testModel.stl");
    engine.save_binary_model(model_data);
    engine.run();
    console.log("Main called!");
    console.log("Printing stderr:");
    console.log(engine.get_stderr());
    console.log("Printing stdout:");
    console.log(engine.get_stdout());
    console.log("Printing status:");
    console.log(engine.get_status());
    console.log("Printing gcode:");
    console.log(engine.get_gcode());
});
