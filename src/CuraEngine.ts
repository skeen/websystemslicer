/// <reference path="ICuraEngine.ts" />

// We need dynamic javascript loading
declare var requirejs;

// The interface for CuraEngineInternal.js
// (The Emscripten CuraEngine)
interface EngineObject
{
    main(argv : string[]) : void;
    save_ascii(ascii : string) : void;
    save_binary(binary : Uint8Array) : void;
    get_gcode() : string;
}

class CuraEngine implements ICuraEngine
{
    // stdout and stderr captured streams as strings
    private stdout : string;
    private stderr : string;
    // Emscripten engine object
    private engine_object : EngineObject;
    // Emscripten / General status stream as string
    private status : string;

    constructor()
    {
        this.stdout = "";
        this.stderr = "";
        this.status = "";
        this.engine_object = null;
    }

    is_loaded() : boolean
    {
        // We're loaded, if the engine object is set
        return this.engine_object !== null;
    }

    load(callback ?: Function) : void
    {
        // Capture this, for our printout functions
        // Setup Emscripten module values for our needs
        var _this = this;
        var Module = {
            // We're just loading, we'll call main ourselves
            noInitialRun: true,
            // We don't want emscripten to tear the system down after running
            // main once, we'd like to be able to rerun multiple times.
            noExitRuntime: true,
            // Capture stdout and stderr in our variables for it
            print: function(text) {
                _this.stdout += text + '\n';
            },
            printErr: function(text) {
                _this.stderr += text + '\n';
            },
            setStatus: function(text) {
                _this.status += text + '\n';
            },
            // TODO: We need to increase the heap size, either here;
            TOTAL_MEMORY: 500000000
            // or when compiling (preferable)
        };

        // Load the file!
        requirejs(["CuraEngineInternal"], function(CuraEngineInternal)
        {
            // Pass the Module argument to our CuraEngineInternal.js
            // This effecively passes it to CureEngineCompiled.js
            var engine_object = CuraEngineInternal(Module);

            // Capture the engine object, i.e. the object to call main on.
            _this.engine_object = engine_object;
            // Call the provided callback handler if any
            if(callback) callback();
        });
    }

    // Run without passing any commandline arguments at all
    raw_run(argv ?: string[], callback ?: Function) : void
    {
        // Check that the engine has been loaded
        if(this.is_loaded() === false) {
            console.error("Engine has not be loaded before run!");
            return;
        }
        // Clear stdout and stderr
        this.stdout = "";
        this.stderr = "";
        this.status = "";
        // Call the main function
        this.engine_object.main(argv);
        // Do the callback
        if(callback) callback();
    }

    // Run passing default commandline arguments
    run(callback ?: Function) : void
    {
        this.raw_run(['a.stl', '-o', 'a.gcode'], callback);
    }

    get_stdout() : string
    {
        return this.stdout;
    }

    get_stderr() : string
    {
        return this.stderr;
    }

    get_status() : string
    {
        return this.status;
    }

    save_ascii_model(ascii : string, callback ?: Function) : void
    {
        // Check that the engine has been loaded
        if(this.is_loaded() === false) {
            console.error("Engine has not be loaded before saving model!");
            return;
        }
        this.engine_object.save_ascii(ascii);
        // Do the callback
        if(callback) callback();
    }

    save_binary_model(binary : Uint8Array, callback ?: Function) : void
    {
        // Check that the engine has been loaded
        if(this.is_loaded() === false) {
            console.error("Engine has not be loaded before saving model!");
            return;
        }
        this.engine_object.save_binary(binary);
        // Do the callback
        if(callback) callback();
    }

    get_gcode() : string
    {
        // Check that the engine has been loaded
        if(this.is_loaded() === false) {
            console.error("Engine has not be loaded before loading gcode!");
            return;
        }
   
        return this.engine_object.get_gcode();
    }
}
