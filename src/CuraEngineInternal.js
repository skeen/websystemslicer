define([], function () {
    // Module is provided when the library has been loaded
    return function(Module) {
    // NOTE: res/CuraEngineCompiled.js will inserted here, during build:
    //
    // INSERT-CURA-ENGINE-COMPILED
    //
    // NOTE: At this point we're back in CuraEngineInternal.js

    return {
        // Call the main function of the engine
        main: function(argv) {
            try {
                Module['callMain'](argv);
            }
            catch(err) {
                Module['setStatus']("Error while running main!" + err);
            }
        },
        save_x : function(data, data_encoding) {
            try {
                FS.writeFile('a.stl', data, { encoding: data_encoding});
            }
            catch(err) {
                Module['setStatus']("Invalid attempt at writing a.stl!" + err);
            }
        },
        // Save an ascii stl file to the virtual file system
        save_ascii: function(ascii) {
            this.save_x(ascii, 'utf8');
        },
        save_binary: function(binary) {
            this.save_x(binary, 'binary');
        },
        // Read the gcode output from the virtual file system
        get_gcode: function() {
            try {
                return FS.readFile('a.gcode', { encoding: 'utf8' });
            }
            catch(err) {
                Module['setStatus']("Invalid attempt at reading a.gcode!" + err);
                return "";
            }
        }
    };
    }
});
