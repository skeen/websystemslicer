CLOSURE_COMPILER = java -jar tools/compiler.jar
TYPESCRIPT_COMPILER = tsc
NODE_JS = node
# EMSCRIPTEN is used recursively and should be set as an environment variable:
# set -x EMSCRIPTEN ../../tools/emscripten/emscripten/master
export EMSCRIPTEN = ../../tools/emscripten/emscripten/master

all: release

get_tools:
	make -f ToolsMakefile

clean:
	rm -rf build/

clean-all: clean
	rm res/CuraEngineCompiled.js

setup: get_tools
	mkdir -p build/

setup_node: setup
	mkdir -p build/node

setup_browser: setup
	mkdir -p build/browser

setup_worker: setup
	mkdir -p build/worker

build/node/node_run.js: setup_node
	cp src/node/node_run.js build/node/node_run.js

build/worker/worker_run.html: setup_worker
	cp src/worker/worker_run.html build/worker/worker_run.html

build/browser/browser_run.html: setup_browser
	cp src/browser/browser_run.html build/browser/browser_run.html

build/worker/CuraEngineWebWorker.js: setup_worker
	cp src/worker/CuraEngineWebWorker.js build/worker/CuraEngineWebWorker.js

build/testModel.stl: setup
	cp res/testModel.stl build/testModel.stl

build/CuraEngine.js: setup
	$(TYPESCRIPT_COMPILER) src/CuraEngine.ts -out build/CuraEngine.js

build/node/CuraEngineTest.js: setup_node
	$(TYPESCRIPT_COMPILER) src/node/CuraEngineTest.ts -out build/node/CuraEngineTest.js

build/worker/CuraEngineWorker.js: setup_worker
	$(TYPESCRIPT_COMPILER) src/worker/CuraEngineWorker.ts -out build/worker/CuraEngineWorker.js

res/CuraEngineCompiled.js:
	make -C res

build/CuraEngineInternal.js: res/CuraEngineCompiled.js setup
	sed -e '/INSERT-CURA-ENGINE-COMPILED/ {' -e 'r res/CuraEngineCompiled.js' -e 'd' -e '}' src/CuraEngineInternal.js > build/CuraEngineInternal.js

compile: build/CuraEngine.js
build: compile build/CuraEngineInternal.js

compile_node:build build/node/CuraEngineTest.js build/node/node_run.js
	
compile_browser: build build/browser/browser_run.html

compile_worker: build build/worker/worker_run.html build/worker/CuraEngineWebWorker.js build/worker/CuraEngineWorker.js

test_node: compile_node build/testModel.stl
	cp build/CuraEngine.js build/node/CuraEngine.js
	cp build/CuraEngineInternal.js build/node/CuraEngineInternal.js
	$(NODE_JS) build/node/node_run.js

test_browser: compile_browser
	cp build/CuraEngine.js build/browser/CuraEngine.js
	cp build/CuraEngineInternal.js build/browser/CuraEngineInternal.js
	
test_worker: compile_worker
	cp build/CuraEngine.js build/worker/CuraEngine.js
	cp build/CuraEngineInternal.js build/worker/CuraEngineInternal.js

setup_release: setup
	mkdir -p build/release/

build/release/CuraEngine.js: setup_release build/CuraEngine.js
	$(CLOSURE_COMPILER) build/CuraEngine.js > build/release/CuraEngine.js

build/release/CuraEngineInternal.js: setup_release build/CuraEngineInternal.js
	$(CLOSURE_COMPILER) build/CuraEngineInternal.js > build/release/CuraEngineInternal.js

release: build/release/CuraEngine.js build/release/CuraEngineInternal.js
